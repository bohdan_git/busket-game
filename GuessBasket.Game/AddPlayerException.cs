﻿using System;

namespace GuessBasket.Game
{
    public class AddPlayerException : Exception
    {
        private const string message = "Can`t add player to Dictionary.";

        public AddPlayerException() : base(message)
        {
        }
    }
}
