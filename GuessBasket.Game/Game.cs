﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using GuessBasket.Game.Players;

namespace GuessBasket.Game
{
    public sealed class Game : IDisposable
    {
        private static readonly Random Random = new Random();

        private readonly object _lockObject = new object();
        private readonly CancellationTokenSource _cancellationToken;

        private readonly Action<int> _gameStarted;
        private readonly Action<BasePlayer, int, int> _gameFinished;

        private int _basketWeight;

        private ConcurrentDictionary<BasePlayer, List<int>> Results { get; }

        private Game(Action<int> gameStarted, Action<BasePlayer, int, int> gameFinished)
        {
            _gameStarted = gameStarted;
            _gameFinished = gameFinished;

            Results = new ConcurrentDictionary<BasePlayer, List<int>>();
            _cancellationToken = new CancellationTokenSource();
        }

        public static void Start(Action<int> gameStarted, Action<BasePlayer, int, int> gameFinished)
        {
            var game = new Game(gameStarted, gameFinished);

            Task.WaitAll(Task.Run(() => game.Start()));
        }

        private void Start()
        {
            InitGame();

            var players = new List<Task>();

            foreach (var player in Results.Keys)
            {
                players.Add(Task.Run(() => player.TryGuess(_basketWeight, _cancellationToken), _cancellationToken.Token));
            }

            _cancellationToken.Token.Register(EndOfGame, true);
            _cancellationToken.CancelAfter(GameConstants.MaxTime);

            Task.WaitAny(players.ToArray());
        }

        private void InitGame()
        {
            _basketWeight = Random.Next(GameConstants.MinWeight, GameConstants.MaxWeight + 1);
            _gameStarted.Invoke(_basketWeight);
            CreatePlayers();
        }

        private void EndOfGame()
        {
            var closest = Results.SelectMany(x => x.Value).Aggregate((x, y) => Math.Abs(x - _basketWeight) < Math.Abs(y - _basketWeight) ? x : y);
            var winner = Results.FirstOrDefault(x => x.Value.Contains(closest));
            var count = Results.Sum(x => x.Value.Count);

            _gameFinished.Invoke(winner.Key, closest, count);
        }

        private void AddAnswer(BasePlayer player, int answer)
        {
            lock (_lockObject)
            {
                if (!_cancellationToken?.IsCancellationRequested == true)
                {
                    Results[player].Add(answer);
                    if (Results.Sum(x => x.Value.Count) >= GameConstants.MaxAttempts)
                    {
                        _cancellationToken.Cancel();
                    }
                }
            }
        }

        private bool IsUniqAnswer(BasePlayer player, int answer)
        {
            if (player != null)
            {
                return Results[player].All(a => a != answer);
            }

            return !Results.Any(r => r.Value.Contains(answer));
        }

        private void CreatePlayers()
        {
            var palyersCount = Random.Next(GameConstants.MinPlayers, GameConstants.MaxPlayers + 1);

            while (palyersCount > 0)
            {
                var playerType = (PlayerType)Random.Next(Enum.GetNames(typeof(PlayerType)).Length);
                var playerName = $"Player {palyersCount}";
                var player = PlayerFactory.CreatePlayer(playerType, playerName, AddAnswer, IsUniqAnswer);
                --palyersCount;

                if (!Results.TryAdd(player, new List<int>()))
                {
                    throw new AddPlayerException();
                }
            }
        }

        public void Dispose()
        {
            _cancellationToken.Cancel();
            _cancellationToken.Dispose();
        }
    }
}
