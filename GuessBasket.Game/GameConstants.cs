﻿namespace GuessBasket.Game
{
    //Game Constants 
    public static class GameConstants
    {
        public static int MaxWeight => 140;

        public static int MinWeight => 40;

        public static int MaxAttempts => 100;

        public static int MaxPlayers => 8;

        public static int MinPlayers => 2;

        public static int MaxTime => 1500;
    }
}
