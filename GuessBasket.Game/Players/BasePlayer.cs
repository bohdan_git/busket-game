﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace GuessBasket.Game.Players
{
    public abstract class BasePlayer : IPlayer
    {
        private readonly Action<BasePlayer, int> _addAnswer;

        public string Name { get; }

        public List<int> Answers;

        protected BasePlayer(string playerName, Action<BasePlayer, int> addAnswer)
        {
            Name = playerName;
            Answers = new List<int>();
            _addAnswer = addAnswer;
        }

        public abstract int GuessWeight();

        public void TryGuess(int realWeight, CancellationTokenSource token)
        {
            while (!token.IsCancellationRequested)
            {
                var answer = GuessWeight();
                _addAnswer.Invoke(this, answer);

                if (answer != realWeight)
                {
                    var diff = realWeight - answer;
                    Task.Delay(diff > 0 ? diff : -diff);
                }
                else
                {
                    token.Cancel();
                    break;
                }
            }
        }
    }
}
