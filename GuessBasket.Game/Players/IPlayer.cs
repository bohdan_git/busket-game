﻿using System.Threading;

namespace GuessBasket.Game.Players
{
    public interface IPlayer
    {
        string Name { get; }

        int GuessWeight();

        void TryGuess(int realWeight, CancellationTokenSource token);
    }
}
