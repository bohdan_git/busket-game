﻿using System;

namespace GuessBasket.Game.Players.Implementation
{
    /// <summary>
    ///  guesses a random number between 40 and 140 – but doesn’t try any of the numbers that other players had already tried.
    /// </summary>
    public class CheaterPlayer : ReadingPlayer
    {
        protected readonly Random Random;

        public CheaterPlayer(string playerName, Action<BasePlayer, int> addAnswer, Func<BasePlayer, int, bool> isUniqAnswer)
            : base(playerName, addAnswer, isUniqAnswer)
        {
            Random = new Random();
        }

        public override int GuessWeight()
        {
            var guessWeight = Random.Next(GameConstants.MinWeight, GameConstants.MaxWeight + 1);

            while (CheckQuessWeight(guessWeight))
            {
                guessWeight = Random.Next(GameConstants.MinWeight, GameConstants.MaxWeight) + 1;
            }

            return guessWeight;
        }

        protected virtual bool CheckQuessWeight(int guessWeight)
        {
            return !IsUniqAnswer(null, guessWeight);
        }
    }
}
