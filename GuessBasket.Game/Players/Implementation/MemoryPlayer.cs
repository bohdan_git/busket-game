﻿using System;

namespace GuessBasket.Game.Players.Implementation
{
    /// <summary>
    /// Guesses a random number between 40 and 140 but doesn’t try the same number more than once
    /// </summary>
    public class MemoryPlayer : CheaterPlayer
    {
        public MemoryPlayer(string playerName, Action<BasePlayer, int> addAnswer,
            Func<BasePlayer, int, bool> isUniqAnswer)
            : base(playerName, addAnswer, isUniqAnswer)
        {
        }

        protected override bool CheckQuessWeight(int guessWeight)
        {
            return !IsUniqAnswer(this, guessWeight);
        }
    }
}
