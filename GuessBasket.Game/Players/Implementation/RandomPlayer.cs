﻿using System;

namespace GuessBasket.Game.Players.Implementation
{
    /// <summary>
    /// guesses a random number between 40 and 140
    /// </summary>
    public class RandomPlayer : BasePlayer
    {
        private readonly Random _random;

        public RandomPlayer(string playerName, Action<BasePlayer, int> addAnswer) : base(playerName, addAnswer)
        {
            _random = new Random();
        }

        public override int GuessWeight()
        {
            return _random.Next(GameConstants.MinWeight, GameConstants.MaxWeight + 1);
        }
    }
}
