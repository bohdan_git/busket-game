﻿using System;

namespace GuessBasket.Game.Players.Implementation
{
    /// <summary>
    /// tries all numbers by order – 41, 42, 43 … but skips numbers that were already been tried before by any of the players.
    /// </summary>
    public class ThoroughCheaterPlayer : ReadingPlayer
    {
        private int _guessWeigth;

        public ThoroughCheaterPlayer(string playerName, Action<BasePlayer, int> addAnswer, Func<BasePlayer, int, bool> isUniqAnswer) 
            : base(playerName, addAnswer, isUniqAnswer)
        {
            _guessWeigth = GameConstants.MinWeight;
        }

        public override int GuessWeight()
        {
            while (!IsUniqAnswer.Invoke(null, _guessWeigth))
            {
                ++_guessWeigth;
            }

            return _guessWeigth++;
        }
    }
}
