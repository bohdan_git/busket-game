﻿using System;

namespace GuessBasket.Game.Players.Implementation
{
    /// <summary>
    /// tries all numbers by order – 41,42,43 …
    /// </summary>
    public class ThoroughPlayer : BasePlayer
    {
        private int _guessWeigth;

        public ThoroughPlayer(string playerName, Action<BasePlayer, int> addAnswer) : base(playerName, addAnswer)
        {
            _guessWeigth = GameConstants.MinWeight;
        }

        public override int GuessWeight()
        {
            return _guessWeigth++;
        }
    }
}
