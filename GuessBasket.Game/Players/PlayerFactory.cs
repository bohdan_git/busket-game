﻿using System;
using GuessBasket.Game.Players.Implementation;

namespace GuessBasket.Game.Players
{
    public static class PlayerFactory
    {
        public static BasePlayer CreatePlayer(PlayerType playerType, string playerName, 
            Action<BasePlayer, int> addAnswer, Func<BasePlayer, int, bool> isUniqAnswer)
        {
            BasePlayer player;
      
            switch (playerType)
            {
                case PlayerType.Cheater:
                    player = new CheaterPlayer(playerName, addAnswer, isUniqAnswer);
                    break;
                case PlayerType.Memory:
                    player = new MemoryPlayer(playerName, addAnswer, isUniqAnswer);
                    break;
                case PlayerType.Thorough:
                    player = new ThoroughPlayer(playerName, addAnswer);
                    break;
                case PlayerType.ThoroughCheater:
                    player = new ThoroughCheaterPlayer(playerName, addAnswer, isUniqAnswer);
                    break;
                case PlayerType.Random:
                default:
                    player = new RandomPlayer(playerName, addAnswer);
                    break;
            }

            return player;
        }
    }
}
