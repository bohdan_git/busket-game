﻿namespace GuessBasket.Game.Players
{
    public enum PlayerType
    {
        Random = 0,
        Memory = 1,
        Thorough = 2,
        Cheater = 3,
        ThoroughCheater = 4
    }
}
