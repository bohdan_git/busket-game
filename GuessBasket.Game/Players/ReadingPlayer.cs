﻿using System;

namespace GuessBasket.Game.Players
{
    public abstract class ReadingPlayer : BasePlayer
    {
        protected Func<BasePlayer, int, bool> IsUniqAnswer;

        protected ReadingPlayer(string playerName, Action<BasePlayer, int> addAnswer, Func<BasePlayer, int, bool> isUniqAnswer)
            : base(playerName, addAnswer)
        {
            IsUniqAnswer = isUniqAnswer;
        }
    }
}
