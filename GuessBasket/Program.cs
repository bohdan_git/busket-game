﻿using System;
using GuessBasket.Game.Players;

namespace GuessBasket
{
    class Program
    {
        static void Main(string[] args)
        {
            Game.Game.Start(GameStarted, GameFinished);

            Console.ReadKey();
        }

        public static void GameStarted(int basketWeight)
        {
            Console.WriteLine($"Real Basket Weight is {basketWeight} kilos.");
        }

        public static void GameFinished(BasePlayer player, int answer, int steps)
        {
            Console.WriteLine($"Name: {player.Name} \nType: {player.GetType().Name} \nAnswer: {answer} \nSteps: {steps}");
        }
    }
}